#include "shared.h"
#include <string.h>
#include "picohttpparser.h"

static ERL_NIF_TERM atom_chunked;
static ERL_NIF_TERM atom_incomplete;

static int on_load(ErlNifEnv* env, void** priv, ERL_NIF_TERM info) {
    atom_chunked = make_atom(env, "chunked");
    atom_incomplete = make_atom(env, "incomplete");
    return 0;
}

void to_lower(char* text, size_t len) {
    while (len--)
        if (text[len] >= 'A' && text[len] <= 'Z')
            text[len] += 32;
}

int parse_int(const char* text, size_t len) {
    int value = 0;
    while (len--)
        value = (value * 10) + (*text++) - '0';
    return value;
}

ERL_NIF_TERM http_parse_headers(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]) {
    ErlNifBinary buffer;
    if (!enif_inspect_binary(env, argv[0], &buffer))
        return atom_incomplete;
    
    int version;
    ERL_NIF_TERM erl_content;
    struct phr_header headers[64];
    const char *method, *path, *query;
    size_t pos, method_len, path_len, num_headers;
    
    num_headers = sizeof(headers) / sizeof(headers[0]);
    int parsed = phr_parse_request(
        (const char*) buffer.data, buffer.size,
        &method, &method_len, &path, &path_len,
        &version, (struct phr_header*) &headers, &num_headers, 0
    );

    if (parsed <= 0)
        return atom_incomplete;

    pos = ((uint8_t*) method) - buffer.data;
    ERL_NIF_TERM erl_method = enif_make_sub_binary(env, argv[0], pos, method_len);

    pos = ((uint8_t*) path) - buffer.data;
    ERL_NIF_TERM erl_path = enif_make_sub_binary(env, argv[0], pos, path_len));
    
    ERL_NIF_TERM erl_query;
    if ((query = memchr(path, '?', path_len))) {
        pos = ((uint8_t*) (query + 1)) - buffer.data;
        erl_query = enif_make_sub_binary(env, argv[0], pos, path_len - (query + 1 - path));
    } else {
        erl_query = enif_make_sub_binary(env, argv[0], 0, 0)
    }

    ERL_NIF_TERM erl_headers = enif_make_list(env, 0);
    while (num_headers--) {
        struct phr_header header = headers[num_headers];
        to_lower((char*) header.name, header.name_len);

        if (!strncmp("content-length", header.name, header.name_len))
            erl_content = enif_make_int(env, parse_int(header.value, header.value_len));
        if (!strncmp("transfer-encoding", header.name, header.name_len)) {
            to_lower((char*) header.value, header.value_len);
            if (!strncmp("chunked", header.value, header.value_len))
                erl_content = atom_chunked;
        }

        pos = ((uint8_t*) header.name) - buffer.data;
        ERL_NIF_TERM erl_name = enif_make_sub_binary(env, argv[0], pos, header.name_len);

        pos = ((uint8_t*) header.value) - buffer.data;
        ERL_NIF_TERM erl_value = enif_make_sub_binary(env, argv[0], pos, header.value_len);

        ERL_NIF_TERM erl_header = enif_make_tuple2(env, erl_name, erl_value);
        erl_headers = enif_make_list_cell(env, erl_header, erl_headers);
    }

    ERL_NIF_TERM remainings = enif_make_sub_binary(env, argv[0], parsed, buffer.size - parsed);
    return enif_make_tuple6(env,
        remainings,
        erl_method,
        erl_path,
        erl_query,
        erl_headers,
        erl_content
    );
}

ERL_NIF_TERM http_parse_chunks(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]) {
    ErlNifBinary buffer;
    if (!enif_inspect_binary(env, argv[0], &buffer))
        return atom_incomplete;

    size_t decode_size = buffer.size;
    struct phr_chunked_decoder decoder = {};
    
    decoder.consume_trailer = 1;
    int parsed = phr_decode_chunked(&decoder, buffer.data, &decode_size);
    if (parsed <= 0)
        return atom_incomplete;

    ERL_NIF_TERM decoded = enif_make_sub_binary(env, argv[0], 0, &decode_size);
    ERL_NIT_TERM remaining = enif_make_sub_binary(env, argv[0], decode_size, buffer.size);
    return enif_make_tuple2(env, decoded, remaining);
}

ERL_NIF_TERM http_serialize_headers(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]) {

}

static ErlNifFunc nif_funcs[] = {
    {"parse_chunks", 1, http_parse_chunks},
    {"parse_headers", 1, http_parse_headers},
    {"serialize_headers", 1, http_serialize_headers}
};

ERL_NIF_INIT(Elixir.DS.Http.Parser, nif_funcs, &on_load, NULL, NULL, NULL);