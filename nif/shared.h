#ifndef DS_SHARED_H
#define DS_SHARED_H

#include <stdint.h>
#include <stddef.h>
#include <erl_nif.h>

inline ERL_NIF_TERM make_atom(ErlNifEnv *env, const char *name) {
    ERL_NIF_TERM ret;
    if (enif_make_existing_atom(env, name, &ret, ERL_NIF_LATIN1))
        return ret;
    return enif_make_atom(env, name);
}

#endif // DS_SHARED_H