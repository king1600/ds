defmodule DS.MixProject do
  use Mix.Project

  def project, do: [
    app: :ds,
    deps: deps(),
    elixir: "~> 1.8",
    version: "0.0.1",
    make_clean: ["clean"],
    start_embedded: Mix.env == :prod,
    build_embedded: Mix.env == :prod,
    compilers: [:elixir_make] ++ Mix.compilers,
  ]

  def application, do: [
    extra_applications: [:logger],
    mod: {DS.Application, []}
  ]

  defp deps, do: [
    {:ranch, "~> 1.7"},
    {:jason, "~> 1.1"},
    {:elixir_make, "~> 0.4", runtime: false},
  ]
end