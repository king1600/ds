SRC_DIR   := nif
BUILD_DIR := priv
OUTPUT    := $(BUILD_DIR)/nif.so

ERL_PATH = $(shell erl -eval 'io:format("~s", [lists:concat([code:root_dir(), "/erts-", erlang:system_info(version), "/include"])])' -s init stop -noshell)
CFLAGS 	+= -g -O2 -shared -fPIC -I$(ERL_PATH)
LDFLAGS +=

SOURCES := $(shell find $(SRC_DIR) -name '*.c' | sort -k 1nr | cut -f2-)
OBJECTS := $(SOURCES:$(SRC_DIR)/%.c=$(BUILD_DIR)/%.o)
DEPS    := $(OBJECTS:.o=.d)

$(OUTPUT): $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $@ $(LDFLAGS)

$(BUILD_DIR)/%.o : $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -MMD -MP -c $< -o $@

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR) && mkdir $(BUILD_DIR)

-include $(DEPS)