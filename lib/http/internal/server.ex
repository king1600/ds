defmodule DS.Http.DefaultServer do
  use DS.Http.Server
end

defmodule DS.Http.Server do
  defmacro __using__(_) do
    quote do
      use GenServer
      require Logger

      def start_link(opts), do:
        GenServer.start_link(__MODULE__, opts, name: __MODULE__)

      def route(method, path, conn), do:
        DS.Http.Conn.send_json(404, %{
          "error" => "There exist no route for #{method} #{path}"
        })

      def init(options) do
        {:ok, _} = :ranch.start_listener(
          DS.Http.TcpServer, 
          :ranch_tcp, options,
          DS.Http.Protocol, [__MODULE__])
        Logger.debug "[DS.Http] Starting http server on :#{options[:port]}"
        {:ok, nil}
      end

      defoverridable route: 3
    end
  end
end