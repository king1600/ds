defmodule DS.Http.Conn do
  alias DS.Http.{Conn, Protocol}

  defstruct(
    protocol: %Protocol{},
    method: "GET",
    path: "/",
    params: %{},
    headers: %{},
    body: <<>>,
    resp_headers: [],
  )

  def add_header(%Conn{resp_headers: headers}=conn, key, value), do:
    %{conn | resp_headers: [{key, value} | headers]}

  def send_json(conn, status, data), do:
    add_header(conn, "content-type", "application/json")
      |> send_data(status, Jason.encode!(data))
  def send_url_form(conn, status, data), do:
    add_header(conn, "content-type", "application/x-www-form-urlencoded")
      |> send_data(status, URI.encode_query(data) |> UI.encode_www_form)

  def send_file(%Conn{protocol: protocol}=conn, status, filename) do
    send_data(conn, status, [])
    protocol.transport.sendfile(protocol.socket, filename)
    %Conn{}
  end

  def send_data(protocol: protocol, resp_headers: headers}=conn, status, content) do
    headers = [{"content-length", Integer.to_string(IO.iodata_length(content))} | headers]
    headers = DS.Http.Parser.serialize_headers(headers)
    status = DS.Http.Parser.get_status_code(status)

    data = ["HTTP/1.1 ", status, headers, "\r\n", content]
    protocol.transport.send(protocol.socket, data)
    %Conn{}
  end
end