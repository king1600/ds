defmodule DS.Http.Protocol do
  use GenServer
  alias DS.Http.{Conn, Protocol}

  defstruct(
    socket: nil,
    router: nil,
    transport: nil,
    state: :headers,
    conn: %Conn{},
    buffer: <<>>,
  )

  def start_link(ref, socket, transport, opts), do:
    {:ok, :proc_lib.spawn_link(__MODULE__, :init, [ref, socket, transport, opts])}

  def init(options), do: {:ok, options}
  def init(ref, socket, transport, opts) do
    :ok = :ranch.accept(ref)
    :ok = transport.setopts(socket, [active: true])
    :gen_server.enter_loop(__MODULE__, [], %Protocol{
      socket: socket,
      transport: transport,
      router: Keyword.get(opts, :router, DS.Http.DefaultServer)
    })
  end

  def handle_info({:tcp_closed, _}, self),
    do: {:stop, :normal, self}
  def handle_info({:tcp_error, _, _reason}, self),
    do: {:noreply, self}

  def handle_info({:tcp, _, data}, %Protocol{state: :headers}=self),
    do: {:noreply, parse_headers(self, self.buffer <> data)}
  def handle_info({:tcp, _, data}, %Protocol{state: :chunked}=self),
    do: {:noreply, parse_chunked(self, self.buffer <> data)}
  def handle_info({:tcp, _, data}, %Protocol{state: _content}=self),
    do: {:noreply, parse_content(self, self.buffer <> data)}

  defp parse_headers(self, buffer), do:
    case DS.Http.Parser.parse_headers(buffer) do
      :incomplete -> 
        %{self | buffer: buffer}
      {remaining, method, path, query_params, headers, next_state} ->
        conn = %Conn{
          method: method, path: path, headers: headers,
          params: URI.decode_query(query_params)
        }
        case next_state do
          :chunked ->
            parse_chunked(%{self | state: :chunked, conn: conn}, remaining)
          content_size when content_size > byte_size(remaining) ->
            parse_content(%{self | state: content_size, conn: conn}, remaining)
          _ ->
            handle_conn(%{self | buffer: remaining}, decode_conn(conn))
        end
    end

  defp parse_chunked(self, buffer), do:
    case DS.Http.Parser.parse_chunks(buffer) do
      {body, remaining} ->
        handle_conn(%{self | buffer: remaining}, decode_conn(%{conn | body: body}))
      :incomplete -> 
        %{self | buffer: buffer}
    end
  
  defp parse_content(%Protocol{state: content_size}=self, buffer), do:
    case buffer do
      <<body :: binary-size(content_size)>> <> remaining ->
        handle_conn(%{self | buffer: remaining}, decode_conn(%{conn | body: body}))
      _ ->
        %{self | buffer: buffer}
    end

  defp decode_conn(%Conn{headers: headers, body: body}=conn), do:
    case Keyword.get(headers, "content-type") do
      ct if String.starts_with?(ct, "application/x-www-form-urlencoded") ->
        %{conn | body: URI.decode_www_form(body) |> URI.decode_query}
      ct if String.starts_with?(ct, "application/json") ->
        %{conn | body: Jason.decode!(body)}
      _ -> conn
    end

  defp handle_conn(%Protocol{router: route}=self, %Conn{method: method, path: path}=conn) do
    route(method, path, conn)
    %Protocol{
      buffer: self.buffer,
      socket: self.socket,
      router: self.router,
      transport: self.transport,
    }
  end
end