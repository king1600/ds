defmodule DS.Application do
  use Application

  def start(_type, _args) do
    children = [
      DS.Http.Server
    ]

    Supervisor.start_link(children, [
      name: DS.Application.Supervisor,
      strategy: :one_for_one,
    ])
  end
end